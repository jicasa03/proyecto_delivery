/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 100138
 Source Host           : localhost:3306
 Source Schema         : menu

 Target Server Type    : MySQL
 Target Server Version : 100138
 File Encoding         : 65001

 Date: 26/08/2020 07:40:12
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for atributo
-- ----------------------------
DROP TABLE IF EXISTS `atributo`;
CREATE TABLE `atributo`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `variables` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `estado` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of atributo
-- ----------------------------
INSERT INTO `atributo` VALUES (1, 'Tamaño', 'Grande, Mediano, Pequeño', 1);
INSERT INTO `atributo` VALUES (2, 'Tipo', 'Vegetariana, Con Carne,Sin nada', 1);
INSERT INTO `atributo` VALUES (3, 'sdfsdf', 'sdfsdf', 0);
INSERT INTO `atributo` VALUES (4, 'Prueba de atribut', 'atributo 1, atrbuto 2, atriuto 2', 0);
INSERT INTO `atributo` VALUES (5, 'Nuevo', 'Nuevo1, Nuevo2,Nuevo3', 1);
INSERT INTO `atributo` VALUES (6, 'Nuevo', 'hola1,hola2,hola3,Hola4', 1);
INSERT INTO `atributo` VALUES (7, 'Prueba', 'Preba1,Prueba2', 1);
INSERT INTO `atributo` VALUES (8, 'Presa', 'Pecho,Ala,Pierna,Entrepierna', 1);

-- ----------------------------
-- Table structure for categoria
-- ----------------------------
DROP TABLE IF EXISTS `categoria`;
CREATE TABLE `categoria`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `class` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `orden` int(11) NULL DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of categoria
-- ----------------------------
INSERT INTO `categoria` VALUES (1, 'General', 'General', 3, 1);
INSERT INTO `categoria` VALUES (2, 'Prueba', 'Prueba', 1, 1);
INSERT INTO `categoria` VALUES (3, 'Nueva Categoria', 'NuevaCategoria', NULL, 0);
INSERT INTO `categoria` VALUES (4, 'Prueba2', 'Prueba2', NULL, 0);
INSERT INTO `categoria` VALUES (5, 'Nuevo', 'Nuevo', NULL, 0);
INSERT INTO `categoria` VALUES (6, 'La wea', 'La wea', NULL, 0);
INSERT INTO `categoria` VALUES (7, 'Pollo Broaster', 'PolloBroaster', 2, 1);
INSERT INTO `categoria` VALUES (8, 'Nuevo', 'Nuevo', 4, 0);
INSERT INTO `categoria` VALUES (9, 'dsfsdf', 'dsfsdf', 5, 0);
INSERT INTO `categoria` VALUES (10, '5345', '5345', 6, 0);

-- ----------------------------
-- Table structure for condicion
-- ----------------------------
DROP TABLE IF EXISTS `condicion`;
CREATE TABLE `condicion`  (
  `id` int(11) NOT NULL,
  `descripcion` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of condicion
-- ----------------------------
INSERT INTO `condicion` VALUES (0, 'Cancelado');
INSERT INTO `condicion` VALUES (1, 'En proceso');
INSERT INTO `condicion` VALUES (2, 'Entregado');

-- ----------------------------
-- Table structure for delivery
-- ----------------------------
DROP TABLE IF EXISTS `delivery`;
CREATE TABLE `delivery`  (
  `delivery_id` int(11) NOT NULL AUTO_INCREMENT,
  `delivery_descripcion` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `delivery_precio` decimal(15, 2) NULL DEFAULT NULL,
  `delivery_estado` int(255) NULL DEFAULT 1,
  PRIMARY KEY (`delivery_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_spanish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of delivery
-- ----------------------------
INSERT INTO `delivery` VALUES (1, 'Tarapoto', 1.00, 1);
INSERT INTO `delivery` VALUES (2, 'morales', 12.00, 1);
INSERT INTO `delivery` VALUES (3, 'banda', 2.00, 0);

-- ----------------------------
-- Table structure for empresa
-- ----------------------------
DROP TABLE IF EXISTS `empresa`;
CREATE TABLE `empresa`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `logo` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `telefono` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `direccion` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `precio_delivery` decimal(8, 2) NULL DEFAULT NULL,
  `hora_entrada` time(0) NULL DEFAULT '08:00:00',
  `hora_salida` time(0) NULL DEFAULT '20:00:00',
  `tiempo_espera` int(11) NOT NULL DEFAULT 0,
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `empresa_estado_imagen` int(255) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of empresa
-- ----------------------------
INSERT INTO `empresa` VALUES (1, 'JIMMY', '23afdff6de1706d3ca58adc289015730.png', '953968612', 'Jr. Lima 978', 2.51, '09:00:00', '23:00:00', 20, 'empresa@gmail.com', 1);

-- ----------------------------
-- Table structure for integracion
-- ----------------------------
DROP TABLE IF EXISTS `integracion`;
CREATE TABLE `integracion`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `telefono` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of integracion
-- ----------------------------
INSERT INTO `integracion` VALUES (1, 'Joyson LD', '+51921920966');
INSERT INTO `integracion` VALUES (2, 'Joyson LD', '+51921920966');
INSERT INTO `integracion` VALUES (3, 'Joyson LD', '+51921920966');
INSERT INTO `integracion` VALUES (4, 'Joyson LD', '+51921920966');
INSERT INTO `integracion` VALUES (5, 'Joyson LD', '+51921920966');
INSERT INTO `integracion` VALUES (6, 'Roman Saavedra Torres', '+51980152791');
INSERT INTO `integracion` VALUES (7, 'Roman Saavedra Torres', '+51980152791');

-- ----------------------------
-- Table structure for metodo_pago
-- ----------------------------
DROP TABLE IF EXISTS `metodo_pago`;
CREATE TABLE `metodo_pago`  (
  `metodo_pago_id` int(11) NOT NULL AUTO_INCREMENT,
  `metodo_pago_descripcion` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `metodo_pago_qr` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `metodo_pago_tipo` int(255) NULL DEFAULT NULL,
  `metodo_pago_estado` int(255) NULL DEFAULT 1,
  PRIMARY KEY (`metodo_pago_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8 COLLATE = utf8_spanish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of metodo_pago
-- ----------------------------
INSERT INTO `metodo_pago` VALUES (1, 'Pago en efectivo', '', 1, 1);
INSERT INTO `metodo_pago` VALUES (14, 'pago con tarjeta', '', 2, 1);
INSERT INTO `metodo_pago` VALUES (15, 'Pago con yape', '108159593_2580719008909994_2847503472101545957_n.jpg', 2, 1);
INSERT INTO `metodo_pago` VALUES (16, 'pago plin', '108159593_2580719008909994_2847503472101545957_n.jpg', 2, 1);

-- ----------------------------
-- Table structure for motorizado
-- ----------------------------
DROP TABLE IF EXISTS `motorizado`;
CREATE TABLE `motorizado`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of motorizado
-- ----------------------------
INSERT INTO `motorizado` VALUES (1, 'ROman', 1);
INSERT INTO `motorizado` VALUES (2, 'Jakeline Natalia Saavedra', 1);
INSERT INTO `motorizado` VALUES (3, 'Ronald Reynaldo Varela', 1);

-- ----------------------------
-- Table structure for pedido
-- ----------------------------
DROP TABLE IF EXISTS `pedido`;
CREATE TABLE `pedido`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `cliente` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `telefono` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `direccion` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `metodo_pago` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `referencia` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `total` decimal(8, 2) NULL DEFAULT NULL,
  `efectivo` decimal(8, 2) NULL DEFAULT NULL,
  `condicion` int(11) NOT NULL DEFAULT 1,
  `delivery` decimal(8, 2) NULL DEFAULT NULL,
  `motorizado` int(11) NULL DEFAULT NULL,
  `hora_entrega` datetime(0) NOT NULL,
  `estado` int(11) NOT NULL DEFAULT 1,
  `metodo_pago_id` int(11) NULL DEFAULT NULL,
  `delivery_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `dasdasd`(`metodo_pago_id`) USING BTREE,
  INDEX `xasdasd`(`delivery_id`) USING BTREE,
  CONSTRAINT `dasdasd` FOREIGN KEY (`metodo_pago_id`) REFERENCES `metodo_pago` (`metodo_pago_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `xasdasd` FOREIGN KEY (`delivery_id`) REFERENCES `delivery` (`delivery_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 95 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of pedido
-- ----------------------------
INSERT INTO `pedido` VALUES (1, '2020-04-07 06:00:00', 'Roman Saavedra Torres', '980152791', 'Josue Saavedra 305', '', '', NULL, NULL, 1, NULL, NULL, '2020-05-27 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (2, '2020-04-13 12:26:53', 'sd', 'asd', 'ads', '', '', NULL, NULL, 1, NULL, NULL, '2020-05-27 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (3, '2020-04-13 12:56:51', 'ROman Saavedra', '980152791', 'Josue Saavedra 305', '', '', NULL, NULL, 1, NULL, NULL, '2020-05-27 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (4, '2020-04-13 12:57:18', 'ROman Saavedra', '980152791', 'Josue Saavedra 305', '', '', NULL, NULL, 1, NULL, NULL, '2020-05-27 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (5, '2020-04-14 02:13:10', 'Toman Saavedra Torres', '98015279', 'Josué aaavedra ', '', '', NULL, NULL, 1, NULL, NULL, '2020-05-27 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (6, '2020-04-14 02:13:55', 'dfsdf', 'sdfsdf', 'sdfsdf', '', '', NULL, NULL, 1, NULL, NULL, '2020-05-27 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (7, '2020-04-14 02:15:27', 'Bshssh', 'Shshsh', 'Hajejeje', '', '', NULL, NULL, 1, NULL, NULL, '2020-05-27 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (8, '2020-04-14 09:16:40', 'Hahah', 'Bsbs', 'Abana', '', '', NULL, NULL, 1, NULL, NULL, '2020-05-27 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (9, '2020-04-14 09:27:41', 'Nuevo', 'Bueno', 'Bsbsn', '', '', NULL, NULL, 1, NULL, NULL, '2020-05-27 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (10, '2020-04-14 09:34:56', 'Juevo', 'Nuevo', 'Bueno', '', '', NULL, NULL, 1, NULL, NULL, '2020-05-27 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (11, '2020-04-14 09:35:46', 'Nombr', 'Jsjej', 'Jejej', '', '', NULL, NULL, 1, NULL, NULL, '2020-05-27 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (12, '2020-04-14 09:38:07', 'Hsbs', 'Hehsh', 'Nsnsn', '', '', NULL, NULL, 1, NULL, NULL, '2020-05-27 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (13, '2020-04-14 09:45:11', 'sdf', 'sdf', 'sdf', '', '', NULL, NULL, 1, NULL, NULL, '2020-05-27 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (14, '2020-04-14 09:45:42', 'Jgu', 'Jchcy', 'Gugu', '', '', NULL, NULL, 1, NULL, NULL, '2020-05-27 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (15, '2020-04-14 09:47:35', 'Roman', '980152791', 'Josué Saavedra 305', '', '', NULL, NULL, 1, NULL, NULL, '2020-05-27 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (16, '2020-04-14 09:57:20', 'sdf', 'sdf', 'sdf', '', '', NULL, NULL, 1, NULL, NULL, '2020-05-27 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (17, '2020-04-16 12:04:51', 'Thh', 'Jji', 'Hji', '', '', NULL, NULL, 1, NULL, NULL, '2020-05-27 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (18, '2020-04-17 17:48:45', 'Hdyxy', 'Cuct', 'Vhcyx', '', '', NULL, NULL, 1, NULL, NULL, '2020-05-27 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (19, '2020-04-21 20:05:10', 'Roman Saavedra', '123456987', 'Jisue Saavedra ', 'POS', 'COlegil Esla perea', NULL, NULL, 1, NULL, NULL, '2020-05-27 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (20, '2020-04-28 20:14:36', 'ROman', '980152791', 'MI casa', 'Efectivo', 'Mic asita', NULL, NULL, 1, NULL, NULL, '2020-05-27 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (21, '2020-04-28 20:16:10', 'rPMAN', '5467', 'CASA', 'Efectivo', 'CASAS', NULL, NULL, 1, NULL, NULL, '2020-05-27 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (22, '2020-04-28 20:16:58', '3231', '3123', '132', 'Efectivo', '123', NULL, NULL, 1, NULL, NULL, '2020-05-27 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (23, '2020-04-29 11:30:41', 'Roman ', '546789', 'swer', 'Efectivo', 'werwer', NULL, NULL, 1, NULL, NULL, '2020-05-27 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (24, '2020-04-29 11:30:47', 'Roman ', '546789', 'swer', 'Efectivo', 'werwer', NULL, NULL, 1, NULL, NULL, '2020-05-27 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (25, '2020-04-29 11:31:55', 'asdasdas', 'adsasd', 'sadasd', 'Efectivo', 'ads', NULL, NULL, 1, NULL, NULL, '2020-05-27 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (26, '2020-04-29 11:39:14', '23235345', '345345', '345435', 'Efectivo', '3455', NULL, NULL, 1, NULL, NULL, '2020-05-27 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (27, '2020-04-29 18:28:03', 'ROman Saavedra', '980152791', 'Josue Saavedra', 'Efectivo', 'COlegio', NULL, NULL, 1, 2.50, NULL, '2020-05-27 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (28, '2020-04-29 18:28:08', 'ROman Saavedra', '980152791', 'Josue Saavedra', 'Efectivo', 'COlegio', NULL, NULL, 1, 2.50, NULL, '2020-05-27 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (29, '2020-04-29 18:42:19', 'Roman', '98765667', 'casa', 'Efectivo', 'casa', NULL, NULL, 1, 2.50, NULL, '2020-05-27 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (30, '2020-04-29 20:04:03', 'Jakeline', '980152791', 'Josue Saavedra', 'Efectivo', 'Coelgio', NULL, NULL, 2, 2.50, NULL, '2020-05-27 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (31, '2020-04-30 00:31:51', 'Roman', 'qweqwe', 'qweqwe', 'Efectivo', 'qweqwe', 38.00, NULL, 1, 2.50, NULL, '2020-05-27 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (32, '2020-04-30 01:18:00', 'Reynaldo', '23234', 'casa', 'Efectivo', 'casa', 17.50, 17.50, 1, 2.50, NULL, '2020-05-27 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (33, '2020-04-30 01:20:54', 'reynaldo', '2323', '234', 'Efectivo', '234', 17.50, NULL, 1, 2.50, NULL, '2020-05-27 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (34, '2020-04-30 01:22:49', '234234', '234234', '234', 'Efectivo', '234234', 17.50, NULL, 1, 2.50, NULL, '2020-05-27 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (35, '2020-04-30 01:24:50', '234234', '234234', '234', 'Efectivo', '234234', 2.50, 2.50, 1, 2.50, NULL, '2020-05-27 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (36, '2020-04-30 01:25:10', 'wer', 'wer', 'wer', 'Efectivo', 'wer', 17.50, 20.00, 1, 2.50, 2, '2020-05-27 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (37, '2020-04-30 01:29:32', '234234', '234234', '23423', 'Efectivo', '234234', 17.50, 20.00, 1, 2.50, 3, '2020-05-27 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (41, '2020-04-30 01:31:32', 'roman', '234345', '324234', 'Efectivo', '2344', 17.50, 20.00, 1, 2.50, 2, '2020-05-27 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (42, '2020-04-30 01:32:38', 'hey', '234234', 'e55', 'POS', '3453', 17.50, 17.50, 1, 2.50, 1, '2020-05-27 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (43, '2020-05-01 13:43:18', 'Roman', '970709', 'Josué saavedra', 'Efectivo', 'Colegio está perwsa ', 72.00, 100.00, 1, 2.50, 3, '2020-05-27 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (44, '2020-05-01 21:07:18', 'ROman', '980152791', 'casa', 'Efectivo', 'casa', 93.10, 500.00, 1, 2.50, 3, '2020-05-27 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (47, '2020-05-01 21:47:05', 'roma12', '123', '123', 'POS', '123', 23.00, 23.00, 0, 2.50, 1, '2020-05-27 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (48, '2020-05-04 22:26:55', 'sonia', '2434', 'calle', 'Efectivo', 'nada', 23.00, 100.00, 1, 2.50, NULL, '2020-05-27 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (49, '2020-05-04 22:27:35', 'jakeline', '54646', '54654', 'Efectivo', '54', 36.50, 50.00, 2, 2.50, 1, '2020-05-27 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (50, '2020-05-29 10:18:28', 'Roman', '980152791', 'calle1', 'Efectivo', 'REFERENCIA', 17.51, 100.00, 1, 2.51, NULL, '0000-00-00 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (51, '2020-05-29 10:21:49', 'roman', '9801796', 'calle1', 'Efectivo', 'referencia1', 17.51, 50.00, 1, 2.51, NULL, '0000-00-00 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (52, '2020-05-29 15:56:43', 'roman', '8974654', 'calle 1', 'Efectivo', 'referencia', 32.51, 50.00, 1, 2.51, NULL, '0000-00-00 00:00:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (53, '2020-06-01 12:14:03', 'Roman Saavedra', '980152791', 'calle2', 'Efectivo', 'REFERENCIAs', 38.01, 100.00, 1, 2.51, 2, '2020-06-02 14:25:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (54, '2020-06-01 12:16:12', 'jakeline', '6546546', 'calle', 'Efectivo', 'refe', 38.01, 20.00, 1, 2.51, NULL, '2020-06-01 09:19:15', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (55, '2020-06-01 17:23:29', 'SONIA TORRES', '98632514', 'JOSUE SAAVEDRA 306', 'Efectivo', 'COLEGIO', 38.01, 100.00, 1, 2.51, NULL, '2020-06-03 12:53:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (56, '2020-06-02 09:17:12', 'Luisa', '98653274', 'calle', 'Efectivo', 'referecia', 53.01, 80.00, 1, 2.51, 1, '2020-06-02 09:40:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (57, '2020-06-08 15:02:47', 'weqwe', '3234', 'qwe', 'Efectivo', 'qwe', 17.51, 17.51, 1, 2.51, NULL, '2020-06-08 15:20:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (58, '2020-06-08 15:04:21', 'sdfdsf', '345', '345', 'Efectivo', '345', 17.51, 17.51, 1, 2.51, NULL, '2020-06-08 15:23:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (59, '2020-06-08 15:04:59', '345', '345', '345', 'Efectivo', '345', 17.51, 17.51, 1, 2.51, NULL, '2020-06-08 15:24:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (60, '2020-06-08 15:07:28', '23423', '324234', '234', 'Efectivo', '234', 17.51, 17.51, 1, 2.51, NULL, '2020-06-08 15:26:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (61, '2020-06-08 15:07:38', '234', '234', '234', 'Efectivo', '234', 17.51, 17.51, 1, 2.51, NULL, '2020-06-08 15:27:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (62, '2020-06-08 15:42:43', '345', '345', '345', 'Efectivo', '45', 17.51, 17.51, 1, 2.51, NULL, '2020-06-08 16:02:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (63, '2020-06-08 15:56:14', '234', '234', '234', 'Efectivo', '234', 17.51, 17.51, 1, 2.51, NULL, '2020-06-09 16:05:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (64, '2020-06-08 15:57:02', '234', '345', '535', 'Efectivo', '345', 17.51, 17.51, 1, 2.51, NULL, '2020-06-09 09:30:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (65, '2020-06-16 10:12:41', 'romna', '387428347', 'mi casa ', 'Efectivo', 'csa', 38.01, 100.00, 1, 2.51, NULL, '2020-06-16 10:32:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (66, '2020-06-16 10:28:42', '234234', '234', '2343', 'Efectivo', '4234', 17.51, 2343.00, 1, 2.51, NULL, '2020-06-16 10:48:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (67, '2020-06-16 10:28:45', '234234', '234', '2343', 'Efectivo', '4234', 2.51, 2343.00, 1, 2.51, NULL, '2020-06-16 10:48:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (68, '2020-06-16 10:28:49', '234234', '234', '2343', 'Efectivo', '4234', 2.51, 2343.00, 1, 2.51, NULL, '2020-06-16 10:49:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (69, '2020-06-16 10:28:53', '2342344234', '234', '2343', 'Efectivo', '4234', 2.51, 2343.00, 1, 2.51, NULL, '2020-06-16 10:49:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (70, '2020-06-16 10:28:53', '2342344234', '234', '2343', 'Efectivo', '4234', 2.51, 2343.00, 1, 2.51, NULL, '2020-06-16 10:49:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (71, '2020-06-16 10:28:53', '2342344234', '234', '2343', 'Efectivo', '4234', 2.51, 2343.00, 1, 2.51, NULL, '2020-06-16 10:49:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (72, '2020-06-16 10:29:46', 'qw23423', '23432', '234', 'Efectivo', '234', 17.51, 234.00, 1, 2.51, NULL, '2020-06-16 10:49:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (73, '2020-06-16 10:30:08', '234234', '4234', '2342', 'Efectivo', '234234', 17.51, 234.00, 1, 2.51, NULL, '2020-06-16 10:50:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (74, '2020-06-16 10:30:32', '234', '234', '23423', 'Efectivo', '234', 17.51, 234.00, 1, 2.51, NULL, '2020-06-16 10:50:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (75, '2020-06-16 10:32:37', '234', '234', '4234', 'Efectivo', '234234', 17.51, 4234.00, 1, 2.51, NULL, '2020-06-16 10:52:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (76, '2020-06-16 10:34:23', 'roman', '345345345', '345345', 'Efectivo', '345', 17.51, 34.00, 1, 2.51, NULL, '2020-06-16 10:54:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (77, '2020-06-16 10:34:42', 'roman', '345', '345', 'Efectivo', '3453', 17.51, 345.00, 1, 2.51, 3, '2020-06-16 10:54:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (78, '2020-06-18 12:25:16', 'ROMAN', '324234', '234234', 'Efectivo', '234234', 17.51, 100.00, 1, 2.51, 2, '2020-06-18 12:45:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (79, '2020-06-19 07:58:33', 'werwer', '35345', '345345', 'POS', '345345', 17.51, 17.51, 1, 2.51, 1, '2020-06-19 09:20:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (80, '2020-06-19 08:29:13', 'jAKELINE', '345345', '234234', 'Efectivo', '234234', 17.51, 20.00, 1, 2.51, NULL, '2020-06-19 10:43:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (81, '2020-06-19 08:34:22', '234234', '234234', '2343', 'Efectivo', '2343', 17.51, 20.00, 1, 2.51, NULL, '2020-06-19 10:54:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (82, '2020-06-19 08:38:01', '45345', '345345', '5345', 'Efectivo', '345345', 17.51, 50.00, 1, 2.51, NULL, '2020-06-19 10:57:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (83, '2020-06-19 08:39:06', '34545', '345345', '3534', 'POS', '345345', 17.51, 17.51, 1, 2.51, NULL, '2020-06-19 09:58:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (84, '2020-06-19 08:39:30', '234', '234', '234234', 'POS', '23432', 17.51, 17.51, 1, 2.51, NULL, '2020-06-19 09:59:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (85, '2020-08-14 21:08:00', 'dasdasd', '12312312312', 'psje. humberto pinedo 120', 'Efectivo', 'tarpaoto', 29.81, 29.81, 1, 2.51, NULL, '2020-08-14 21:37:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (86, '2020-08-16 19:58:29', 'dasda', '1312312', 'dasdasd', 'Pago en efectivo', 'dasdasd', 29.81, 1.00, 1, 2.51, NULL, '2020-08-16 20:28:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (87, '2020-08-16 20:01:14', 'dasd', '3123123123', 'dasdasd', 'Pago en efectivo', 'dasdasd', 29.81, 50.00, 1, 2.51, NULL, '2020-08-16 20:30:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (88, '2020-08-18 18:59:50', 'mas,d', '4242', 'kfñlskd', 'Pago en efectivo', 'fslñkdf', 3.51, 100.00, 1, 2.51, NULL, '2020-08-18 19:29:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (89, '2020-08-18 19:03:42', 'fsdfsd', '4234234', 'fsdfsfsd', 'Pago en efectivo', 'dfsdf', 3.51, 200.00, 1, 2.51, NULL, '2020-08-18 19:33:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (90, '2020-08-18 19:06:24', 'dasda', '234234', 'dasdasd', 'Pago en efectivo', 'dasdasd', 5.51, 200.00, 1, 2.51, NULL, '2020-08-18 19:36:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (91, '2020-08-18 19:09:48', 'dasd', '312312', 'adsdas', 'Pago en efectivo', 'dasdasd', 3.51, 200.00, 1, 2.51, NULL, '2020-08-18 19:39:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (92, '2020-08-18 19:12:24', 'tert', '34234', 'fsdfs', 'Pago en efectivo', 'ffsdf', 3.51, 200.00, 1, 2.51, NULL, '2020-08-18 19:42:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (93, '2020-08-18 20:05:54', 'Ndnd', '84884737', 'Jdjddd', 'Pago en efectivo', 'Djjdx', 3.51, 200.00, 1, 2.51, NULL, '2020-08-18 20:35:00', 1, NULL, NULL);
INSERT INTO `pedido` VALUES (94, '2020-08-18 20:08:52', 'flasdjf', '32840293849', 'lsjdjfklsdj', 'Pago en efectivo', 'kladjasjd', 3.51, 200.00, 1, 2.51, NULL, '2020-08-18 20:38:00', 1, NULL, NULL);

-- ----------------------------
-- Table structure for producto
-- ----------------------------
DROP TABLE IF EXISTS `producto`;
CREATE TABLE `producto`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `precio` decimal(8, 2) NOT NULL,
  `descripcion` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `imagen` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `orden` int(11) NULL DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of producto
-- ----------------------------
INSERT INTO `producto` VALUES (1, 'Pizaa', 20.50, 'Pizza americana', '67bb357cd23b9be0e34d74c497fcaed2.jpg', 6, 0);
INSERT INTO `producto` VALUES (2, 'Nuevo', 15.00, 'Descripcion nueva', '61085981dc16912fa01108bf1ed9eb3e.png', 2, 0);
INSERT INTO `producto` VALUES (3, 'prueba', 12.00, 'prueba', '5bd5a52a57ead0ce5774ccf735e98648.png', 3, 0);
INSERT INTO `producto` VALUES (17, 'Prueba2f', 12.00, 'prueba2', 'e3d453da2a78e8a7439c3e36da778897.png', 8, 0);
INSERT INTO `producto` VALUES (18, 'aasd', 34.00, 'asdasd', 'd56f758e219fac5356ebc69197a31203.jpg', 16, 0);
INSERT INTO `producto` VALUES (19, 'Pollo a la braza', 8.50, 'Octavo de pollo a la braza', '938b72f160e366618429b5bee17a5d63.png', 5, 0);
INSERT INTO `producto` VALUES (21, 'Pruebaeeex', 12.30, 'detalles xdlol', 'c9537bdab8b528fe6ce0d3a603fbd89a.jpg', 1, 0);
INSERT INTO `producto` VALUES (23, 'Prueba223', 12.30, 'detalle de   ', 'ba4cb9ddb8b4e6d8a2721cebcd2f47a0.png', 7, 0);
INSERT INTO `producto` VALUES (24, 'Salchipollos', 10.00, 'Con papas y salchicha', 'eb11ccf3aabb7ebdd6a836282b70a69c.png', 10, 0);
INSERT INTO `producto` VALUES (25, '345', 345.00, '345', '', 4, 0);
INSERT INTO `producto` VALUES (26, 'dadas', 1.00, 'dadasd', '67ea0f18b96930ee964e606cb6ea752b.jpg', 1, 1);

-- ----------------------------
-- Table structure for producto_atributo
-- ----------------------------
DROP TABLE IF EXISTS `producto_atributo`;
CREATE TABLE `producto_atributo`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_producto` int(11) NOT NULL,
  `id_atributo` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `pk_producto`(`id_producto`) USING BTREE,
  INDEX `pk_atributo`(`id_atributo`) USING BTREE,
  CONSTRAINT `pk_atributo` FOREIGN KEY (`id_atributo`) REFERENCES `atributo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pk_producto` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 226 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of producto_atributo
-- ----------------------------
INSERT INTO `producto_atributo` VALUES (202, 17, 1);
INSERT INTO `producto_atributo` VALUES (209, 24, 1);
INSERT INTO `producto_atributo` VALUES (216, 19, 1);
INSERT INTO `producto_atributo` VALUES (217, 19, 2);
INSERT INTO `producto_atributo` VALUES (218, 19, 5);
INSERT INTO `producto_atributo` VALUES (219, 2, 1);
INSERT INTO `producto_atributo` VALUES (220, 2, 5);
INSERT INTO `producto_atributo` VALUES (221, 3, 2);
INSERT INTO `producto_atributo` VALUES (222, 21, 1);
INSERT INTO `producto_atributo` VALUES (223, 26, 2);
INSERT INTO `producto_atributo` VALUES (224, 26, 7);
INSERT INTO `producto_atributo` VALUES (225, 26, 8);

-- ----------------------------
-- Table structure for producto_categoria
-- ----------------------------
DROP TABLE IF EXISTS `producto_categoria`;
CREATE TABLE `producto_categoria`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_producto` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `pk_producto_categoria_1`(`id_producto`) USING BTREE,
  INDEX `pk_producto_categoria_2`(`id_categoria`) USING BTREE,
  CONSTRAINT `pk_producto_categoria_1` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pk_producto_categoria_2` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 149 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of producto_categoria
-- ----------------------------
INSERT INTO `producto_categoria` VALUES (101, 17, 7);
INSERT INTO `producto_categoria` VALUES (108, 24, 7);
INSERT INTO `producto_categoria` VALUES (125, 1, 2);
INSERT INTO `producto_categoria` VALUES (126, 1, 7);
INSERT INTO `producto_categoria` VALUES (129, 18, 1);
INSERT INTO `producto_categoria` VALUES (130, 18, 2);
INSERT INTO `producto_categoria` VALUES (132, 23, 1);
INSERT INTO `producto_categoria` VALUES (133, 23, 7);
INSERT INTO `producto_categoria` VALUES (135, 25, 7);
INSERT INTO `producto_categoria` VALUES (139, 19, 1);
INSERT INTO `producto_categoria` VALUES (140, 19, 7);
INSERT INTO `producto_categoria` VALUES (141, 2, 1);
INSERT INTO `producto_categoria` VALUES (142, 2, 2);
INSERT INTO `producto_categoria` VALUES (143, 2, 7);
INSERT INTO `producto_categoria` VALUES (144, 3, 1);
INSERT INTO `producto_categoria` VALUES (145, 3, 7);
INSERT INTO `producto_categoria` VALUES (146, 21, 1);
INSERT INTO `producto_categoria` VALUES (147, 21, 7);
INSERT INTO `producto_categoria` VALUES (148, 26, 7);

-- ----------------------------
-- Table structure for producto_pedido
-- ----------------------------
DROP TABLE IF EXISTS `producto_pedido`;
CREATE TABLE `producto_pedido`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cantidad` int(11) NOT NULL,
  `detalles` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `atributos` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `precio` decimal(8, 2) NULL DEFAULT NULL,
  `id_producto` int(11) NOT NULL,
  `id_pedido` int(11) NOT NULL,
  `estado` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `pk_detalle_producto`(`id_producto`) USING BTREE,
  INDEX `pk_detalle_pedido`(`id_pedido`) USING BTREE,
  CONSTRAINT `pk_detalle_pedido` FOREIGN KEY (`id_pedido`) REFERENCES `pedido` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pk_detalle_producto` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 135 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of producto_pedido
-- ----------------------------
INSERT INTO `producto_pedido` VALUES (1, 1, 'sdfsdf', '', NULL, 1, 4, 1);
INSERT INTO `producto_pedido` VALUES (4, 1, 'fsdf', '', NULL, 1, 4, 1);
INSERT INTO `producto_pedido` VALUES (5, 1, 'Lo quiero ahora xD ', '', NULL, 2, 5, 1);
INSERT INTO `producto_pedido` VALUES (7, 1, 'Pruab', '', NULL, 1, 7, 1);
INSERT INTO `producto_pedido` VALUES (8, 1, 'Hehssn', '', NULL, 2, 8, 1);
INSERT INTO `producto_pedido` VALUES (9, 1, 'Nuevo', '', NULL, 2, 9, 1);
INSERT INTO `producto_pedido` VALUES (10, 1, 'Bsjaja', 'Grande,Nuevo1', NULL, 23, 10, 1);
INSERT INTO `producto_pedido` VALUES (11, 1, '', '', NULL, 1, 11, 1);
INSERT INTO `producto_pedido` VALUES (12, 1, 'Hejshs', '', NULL, 1, 12, 1);
INSERT INTO `producto_pedido` VALUES (13, 1, 'ds', '', NULL, 1, 13, 1);
INSERT INTO `producto_pedido` VALUES (14, 1, 'Hshsh', '', NULL, 2, 14, 1);
INSERT INTO `producto_pedido` VALUES (15, 1, 'Corona virussss', '', NULL, 2, 15, 1);
INSERT INTO `producto_pedido` VALUES (17, 1, 'Bsbsbdnd', 'Grande,Nuevo3', NULL, 23, 15, 1);
INSERT INTO `producto_pedido` VALUES (18, 1, 'werwer', '', NULL, 1, 16, 1);
INSERT INTO `producto_pedido` VALUES (19, 1, '', 'Pecho', NULL, 21, 17, 1);
INSERT INTO `producto_pedido` VALUES (20, 1, 'Nfufd', '', NULL, 1, 18, 1);
INSERT INTO `producto_pedido` VALUES (21, 1, 'Gufufyc', ' Mediano', NULL, 24, 18, 1);
INSERT INTO `producto_pedido` VALUES (22, 1, 'nada de la wea xd', '', NULL, 1, 19, 1);
INSERT INTO `producto_pedido` VALUES (23, 1, '', '', NULL, 1, 20, 1);
INSERT INTO `producto_pedido` VALUES (24, 3, '', 'Vegetariana,Nuevo1', NULL, 18, 20, 1);
INSERT INTO `producto_pedido` VALUES (25, 1, '', '', NULL, 1, 21, 1);
INSERT INTO `producto_pedido` VALUES (26, 1, '', '', NULL, 1, 22, 1);
INSERT INTO `producto_pedido` VALUES (27, 1, '', 'Grande,Nuevo1', NULL, 2, 23, 1);
INSERT INTO `producto_pedido` VALUES (28, 1, '', '', NULL, 18, 23, 1);
INSERT INTO `producto_pedido` VALUES (29, 1, '', 'Grande,Nuevo1', NULL, 2, 25, 1);
INSERT INTO `producto_pedido` VALUES (30, 1, '', '', NULL, 18, 25, 1);
INSERT INTO `producto_pedido` VALUES (31, 1, '', 'Grande,Nuevo1', NULL, 2, 26, 1);
INSERT INTO `producto_pedido` VALUES (32, 1, 'Nota', 'Grande,Nuevo1', NULL, 2, 27, 1);
INSERT INTO `producto_pedido` VALUES (33, 1, 'Nada', '', NULL, 1, 27, 1);
INSERT INTO `producto_pedido` VALUES (34, 1, '', 'Grande,Nuevo1', 15.00, 2, 29, 1);
INSERT INTO `producto_pedido` VALUES (35, 1, '', '', 20.50, 1, 29, 1);
INSERT INTO `producto_pedido` VALUES (36, 1, '', 'Grande', 12.30, 21, 30, 1);
INSERT INTO `producto_pedido` VALUES (37, 1, '', 'Vegetariana', 12.00, 3, 30, 1);
INSERT INTO `producto_pedido` VALUES (38, 1, '', 'Grande,Nuevo1', 15.00, 2, 30, 1);
INSERT INTO `producto_pedido` VALUES (39, 1, '', 'Grande,Nuevo1', 15.00, 2, 31, 1);
INSERT INTO `producto_pedido` VALUES (40, 1, '', '', 20.50, 1, 31, 1);
INSERT INTO `producto_pedido` VALUES (41, 1, '', 'Grande,Nuevo1', 15.00, 2, 32, 1);
INSERT INTO `producto_pedido` VALUES (42, 1, '', 'Grande,Nuevo1', 15.00, 2, 33, 1);
INSERT INTO `producto_pedido` VALUES (43, 1, '', 'Grande,Nuevo1', 15.00, 2, 34, 1);
INSERT INTO `producto_pedido` VALUES (44, 1, '', 'Grande,Nuevo1', 15.00, 2, 36, 1);
INSERT INTO `producto_pedido` VALUES (45, 1, '', 'Grande,Nuevo1', 15.00, 2, 37, 1);
INSERT INTO `producto_pedido` VALUES (46, 1, '', 'Grande,Nuevo1', 15.00, 2, 41, 1);
INSERT INTO `producto_pedido` VALUES (47, 1, '', 'Grande,Nuevo1', 15.00, 2, 42, 1);
INSERT INTO `producto_pedido` VALUES (48, 1, '', 'Grande,Nuevo1', 15.00, 2, 43, 1);
INSERT INTO `producto_pedido` VALUES (49, 1, '', '', 34.00, 18, 43, 1);
INSERT INTO `producto_pedido` VALUES (50, 1, '', '', 20.50, 1, 43, 1);
INSERT INTO `producto_pedido` VALUES (51, 1, 'Nuevo', '', 20.50, 1, 44, 1);
INSERT INTO `producto_pedido` VALUES (52, 1, 'nuevo', 'Grande', 10.00, 24, 44, 1);
INSERT INTO `producto_pedido` VALUES (53, 1, ' nueva asdsd', 'Vegetariana', 12.00, 3, 44, 1);
INSERT INTO `producto_pedido` VALUES (54, 1, 'sadsd', 'Grande', 12.30, 21, 44, 1);
INSERT INTO `producto_pedido` VALUES (55, 1, 'e.e.e', 'Grande,Nuevo1', 15.00, 2, 44, 1);
INSERT INTO `producto_pedido` VALUES (56, 1, 'weqe', '', 12.30, 23, 44, 1);
INSERT INTO `producto_pedido` VALUES (57, 1, 'werwer', 'Grande,Vegetariana,Nuevo1', 8.50, 19, 44, 1);
INSERT INTO `producto_pedido` VALUES (58, 1, 'hola', '', 20.50, 1, 47, 1);
INSERT INTO `producto_pedido` VALUES (59, 1, '', '', 20.50, 1, 48, 1);
INSERT INTO `producto_pedido` VALUES (60, 1, '', '', 34.00, 18, 49, 1);
INSERT INTO `producto_pedido` VALUES (61, 1, '', 'Grande,Nuevo1', 15.00, 2, 50, 1);
INSERT INTO `producto_pedido` VALUES (62, 1, '', 'Grande,Nuevo1', 15.00, 2, 51, 1);
INSERT INTO `producto_pedido` VALUES (63, 1, '', 'Grande,Nuevo1', 15.00, 2, 52, 1);
INSERT INTO `producto_pedido` VALUES (64, 1, '', 'Grande,Nuevo1', 15.00, 2, 52, 1);
INSERT INTO `producto_pedido` VALUES (69, 1, 'nueo', 'Grande,Nuevo1', 15.00, 2, 54, 1);
INSERT INTO `producto_pedido` VALUES (70, 1, 'e.e', '', 20.50, 1, 54, 1);
INSERT INTO `producto_pedido` VALUES (76, 1, '', '', 20.50, 1, 53, 1);
INSERT INTO `producto_pedido` VALUES (77, 1, 'Nota de hoy', 'Grande,Nuevo1', 15.00, 2, 53, 1);
INSERT INTO `producto_pedido` VALUES (78, 1, '', '', 20.50, 1, 55, 1);
INSERT INTO `producto_pedido` VALUES (79, 1, 'Nota de hoy', 'Grande,Nuevo1', 15.00, 2, 55, 1);
INSERT INTO `producto_pedido` VALUES (95, 1, 'lol', '', 20.50, 1, 56, 1);
INSERT INTO `producto_pedido` VALUES (96, 1, 'jeje', 'Grande,Nuevo1', 15.00, 2, 56, 1);
INSERT INTO `producto_pedido` VALUES (97, 1, '', 'Grande,Nuevo1', 15.00, 2, 56, 1);
INSERT INTO `producto_pedido` VALUES (98, 1, '', 'Grande,Nuevo1', 15.00, 2, 57, 1);
INSERT INTO `producto_pedido` VALUES (99, 1, '', 'Grande,Nuevo1', 15.00, 2, 58, 1);
INSERT INTO `producto_pedido` VALUES (100, 1, '', 'Grande,Nuevo1', 15.00, 2, 59, 1);
INSERT INTO `producto_pedido` VALUES (101, 1, '', 'Grande,Nuevo1', 15.00, 2, 60, 1);
INSERT INTO `producto_pedido` VALUES (102, 1, '', 'Grande,Nuevo1', 15.00, 2, 61, 1);
INSERT INTO `producto_pedido` VALUES (103, 1, '', 'Grande,Nuevo1', 15.00, 2, 62, 1);
INSERT INTO `producto_pedido` VALUES (104, 1, '', 'Grande,Nuevo1', 15.00, 2, 63, 1);
INSERT INTO `producto_pedido` VALUES (105, 1, '', 'Grande,Nuevo1', 15.00, 2, 64, 1);
INSERT INTO `producto_pedido` VALUES (106, 1, '', '', 20.50, 1, 65, 1);
INSERT INTO `producto_pedido` VALUES (107, 1, 'Nota de hoy', 'Grande,Nuevo1', 15.00, 2, 65, 1);
INSERT INTO `producto_pedido` VALUES (108, 1, '', 'Grande,Nuevo1', 15.00, 2, 66, 1);
INSERT INTO `producto_pedido` VALUES (109, 1, '', 'Grande,Nuevo1', 15.00, 2, 72, 1);
INSERT INTO `producto_pedido` VALUES (110, 1, '', 'Grande,Nuevo1', 15.00, 2, 73, 1);
INSERT INTO `producto_pedido` VALUES (111, 1, '', 'Grande,Nuevo1', 15.00, 2, 74, 1);
INSERT INTO `producto_pedido` VALUES (112, 1, '', 'Grande,Nuevo1', 15.00, 2, 75, 1);
INSERT INTO `producto_pedido` VALUES (113, 1, '', 'Grande,Nuevo1', 15.00, 2, 76, 1);
INSERT INTO `producto_pedido` VALUES (114, 1, '', 'Grande,Nuevo1', 15.00, 2, 77, 1);
INSERT INTO `producto_pedido` VALUES (115, 1, 'JEJEJE', 'Grande,Nuevo1', 15.00, 2, 78, 1);
INSERT INTO `producto_pedido` VALUES (116, 1, 'wad', 'Grande,Nuevo1', 15.00, 2, 79, 1);
INSERT INTO `producto_pedido` VALUES (117, 1, '', 'Grande,Nuevo1', 15.00, 2, 80, 1);
INSERT INTO `producto_pedido` VALUES (118, 1, '3423', 'Grande,Nuevo1', 15.00, 2, 81, 1);
INSERT INTO `producto_pedido` VALUES (119, 1, '567', 'Grande,Nuevo1', 15.00, 2, 82, 1);
INSERT INTO `producto_pedido` VALUES (120, 1, 'rwr', 'Grande,Nuevo1', 15.00, 2, 83, 1);
INSERT INTO `producto_pedido` VALUES (121, 1, 'werwer', 'Grande,Nuevo1', 15.00, 2, 84, 1);
INSERT INTO `producto_pedido` VALUES (122, 1, '', 'Grande', 12.30, 21, 85, 1);
INSERT INTO `producto_pedido` VALUES (123, 1, '', 'Grande,Nuevo1', 15.00, 2, 85, 1);
INSERT INTO `producto_pedido` VALUES (124, 1, '', 'Grande', 12.30, 21, 86, 1);
INSERT INTO `producto_pedido` VALUES (125, 1, '', 'Grande,Nuevo1', 15.00, 2, 86, 1);
INSERT INTO `producto_pedido` VALUES (126, 1, '', 'Grande', 12.30, 21, 87, 1);
INSERT INTO `producto_pedido` VALUES (127, 1, '', 'Grande,Nuevo1', 15.00, 2, 87, 1);
INSERT INTO `producto_pedido` VALUES (128, 1, '', 'Vegetariana,Preba1,Pecho', 1.00, 26, 88, 1);
INSERT INTO `producto_pedido` VALUES (129, 1, '', 'Vegetariana,Preba1,Pecho', 1.00, 26, 89, 1);
INSERT INTO `producto_pedido` VALUES (130, 3, '', 'Vegetariana,Preba1,Pecho', 1.00, 26, 90, 1);
INSERT INTO `producto_pedido` VALUES (131, 1, '', 'Vegetariana,Preba1,Pecho', 1.00, 26, 91, 1);
INSERT INTO `producto_pedido` VALUES (132, 1, '', 'Vegetariana,Preba1,Pecho', 1.00, 26, 92, 1);
INSERT INTO `producto_pedido` VALUES (133, 1, '', 'Vegetariana,Preba1,Pecho', 1.00, 26, 93, 1);
INSERT INTO `producto_pedido` VALUES (134, 1, '', 'Vegetariana,Preba1,Pecho', 1.00, 26, 94, 1);

-- ----------------------------
-- Table structure for usuario
-- ----------------------------
DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `usuario` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `telefono` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `estado` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of usuario
-- ----------------------------
INSERT INTO `usuario` VALUES (1, 'Roman Saavedra Torres', 'Roman', '980152791', 'roman@email.com', '$1$04nCC/We$eCCThU2aIa0m3MWys56sx1', 1);
INSERT INTO `usuario` VALUES (2, 'Prueba', 'prueba', '942614759', 'prueba@email.com', '$2y$10$g.a32IRWzXYoZ3O75g83e.JFwzyqBRn9dPfHlXd4RpjABwBuLIY1a', 0);
INSERT INTO `usuario` VALUES (4, 'Nuevo emememe', 'nuevoXDD', '980152791', 'nuevo@email.com', '$2y$10$6i8nYNQBikG06UyyP2BNaeiG9D0iaQLCTu688wtQ56L8hr5N2rbeC', 1);

-- ----------------------------
-- View structure for getcategories
-- ----------------------------


-- ----------------------------
-- Function structure for f1
-- ----------------------------
DROP FUNCTION IF EXISTS `f1`;
delimiter ;;
CREATE FUNCTION `f1`()
 RETURNS int(11)
  NO SQL 
  DETERMINISTIC
return @f1;
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
DROP VIEW IF EXISTS `getcategories`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `getcategories` AS select group_concat(`c`.`descripcion` separator ' ') AS `GROUP_CONCAT(c.descripcion SEPARATOR ' ')` from (`categoria` `c` join `producto_categoria` `pc` on((`c`.`id` = `pc`.`id_categoria`))) where (`pc`.`id_producto` = `f1`()) ; ;

-- ----------------------------
-- View structure for vw_producto
-- ----------------------------
DROP VIEW IF EXISTS `vw_producto`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `vw_producto` AS select `producto`.`id` AS `id`,`producto`.`orden` AS `orden`,`producto`.`estado` AS `estado`,`producto`.`nombre` AS `nombre`,`producto`.`precio` AS `precio`,`producto`.`descripcion` AS `des`,group_concat(`categoria`.`descripcion` separator ' ') AS `categorias` from ((`producto` left join `producto_categoria` on((`producto_categoria`.`id_producto` = `producto`.`id`))) left join `categoria` on((`categoria`.`id` = `producto_categoria`.`id_categoria`))) group by `producto`.`id` ; ;


