<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ordenpedido extends MY_Logo {
	public $order;

	public function __construct() {
		parent::__construct();

		$this->load->model('orderModel');
		$this->order = new OrderModel;
		if(!isset($_SESSION['carrito'])){
			$_SESSION['carrito']=[];
		}
	}


	public function index()
	{
		$this->output->set_template('order');
		$this->load->js("public/js/pedidos/orders.js");
		$data = [];
		$data['productos'] = $this->order->getProductos();
		$data['categorias'] = $this->order->getCategorias();
		$data["precio_delivery"]=0;
		$data["tam_precio"]=0;
		$delivery=$this->db->query("select * from delivery where delivery_estado=1 order by delivery_precio asc")->result_array();
		if(count($delivery)>0){
           $delivery1=(float)$delivery[0]["delivery_precio"];
           $data["precio_delivery"]=$delivery1;
           $data["tam_precio"]=count($delivery);
		}

		$this->load->view('orders/index',$data);
	}

	public function checkout($id=''){
		$this->output->set_template('order');
		$this->load->css("public/css/single_styles.css");
		$this->load->css("public/css/single_responsive.css");
		$this->load->js("public/js/pedidos/orders.js");
		$this->load->js("public/js/pedidos/checkout.js");
		$data = [];
		$data['productos'] = $this->order->getCarrito($_SESSION['carrito']);
		$delivery=$this->db->query("select * from delivery where delivery_estado=1 order by delivery_precio asc")->result_array();
		$tam_precio=count($delivery);
	//	$data['precio_delivery'] = $this->order->getPrecioDelivery();
		//$data=['']=$this->db->query("");
		$data['precio_delivery']=0;
		$data['tam_precio']=$tam_precio;
		$data["lista_precio"]=$delivery;
		$data["id_delivery"]='';
		if($tam_precio>0)
		{
           if($id=='')
           {
               $data['precio_delivery']=$delivery[0]["delivery_precio"];
               $data["id_delivery"]=$delivery[0]["delivery_id"];
           }
           else{

			$delivery_pre=$this->db->query("select * from delivery where delivery_estado=1 and delivery_id=".$id)->result_array();
               if(count($delivery_pre)>0){

               $data['precio_delivery']=$delivery_pre[0]["delivery_precio"];
               $data["id_delivery"]=$delivery_pre[0]["delivery_id"];


               }else{
					$data['precio_delivery']=$delivery[0]["delivery_precio"];
					$data['precio_delivery']=$delivery[0]["delivery_id"];


               }
             

           }
		}
		$data['metodo_pago'] = $this->order->getMetodoPago();

		$this->load->view('orders/checkout',$data);
	}

	public function getProductosWhere() { 
		$busqueda = $this->input->post('val'); 


$empresa=$this->db->query("select * from empresa")->row_array();
		$data = $this->order->getProductosWhere($busqueda);
		$array["itemSelector"] = '.product-items';
		$isotope =  json_encode($array);
		$band = null;
		$html='<h2 class="inner" id="tableTittle" ></h2>
		<table id="tabla_buscar" class="table cart-list">
		  <thead>
		    <tr>
		      <th>Producto</th>
		      <th><center>Precio</center></th>
		      <th><center>Agregar</center></th>
		    </tr>
		  </thead>';
		$html.='<tbody class="product-grids" data-isotope='.$isotope.'>';
		foreach ($data as  $producto) {
			$band = $producto->categorias;
			$html.='<tr class="product-items '.$producto->categorias.'">
			  <td>';
			  if($empresa["empresa_estado_imagen"]=="1"){

			      $html.='<div class="rstl_img">
			    <a href="#menu_12">
			        <img src="'.base_url().'public/uploads/productos/'.$producto->imagen.'">
			      </a>
			    </div>';
                  }

			   $html.=' <div class="rstl_img_contant">
			      <h5>'.$producto->nombre.'</h5>
			      <p>'.$producto->descripcion.'</p>
			    </div>
			  </td>
			  <td><center><strong>S/ '.$producto->precio.'</strong></center></td>
			  <td class="options">
			    <a class="btn_add" value="'.$producto->id.'" href=""><i class="fa fa-plus-square-o"></i></a>
			  </td>
			</tr>';
		}
		$html.= " </tbody>
			</table>
			<hr>";
		$envio['htmljs'] = $band.'|'.$html; 

		echo json_encode($envio);
	}

	public function get($id) {
			echo json_encode($this->categorias->find_categoria($id));
	}

	public function getProduct($id) {
			$datos=$this->order->getProducto($id);
			$datos->atributos=$this->order->find_atributos($id);
			echo json_encode($datos);
	}

	public function getEmpresa() {
			echo json_encode($this->order->find_empresa());
	}

public function setCarrito(){

	$datos= array(
		'id_producto' => $_POST['id'],
		'cantidad' => $_POST['cantidad'],
		'nota' => $_POST['nota'],
		'precio' => $_POST['precio'],
	);

	if(isset($_POST['atributo'])){
		$datos['atributos']=$_POST['atributo'];
	}else{
		$datos['atributos']=[];
	}

	array_push($_SESSION['carrito'], $datos );

	$response = [];
	$response["productos_carrito"]=[];
	$response["error"] = true;
	$response["productos_carrito"]=$this->order->getCarrito($_SESSION['carrito']);
	$response["carrito"]=sizeof($_SESSION['carrito']);
	echo json_encode($response);
}

public function delProCar(){
	$id=$_POST['id'];
	array_splice($_SESSION['carrito'], $id, 1);
	$response = [];
	$response["error"] = true;
	echo json_encode($response);
}

public function guardarPedido() {

	$total=0;
	foreach ($_SESSION['carrito'] as $producto) {
		$total+=$producto['cantidad'] * $producto['precio'];
	}
	$_POST['total']=$total;

	$res = $this->order->insert_pedido();
	$last = $this->db->order_by('id',"desc")->limit(1)->get('pedido')->row_array();
	if($res){
		$this->setDetallePedido($_SESSION['carrito'], $last['id']);
	}

	$cel=$this->order->getCelular();
	$_SESSION['carrito']=[];
	$response = [];
	$response["tel"] = $cel->telefono;
	echo json_encode($response);
}

public function editarPedido() {

	$total=0;
	foreach ($_SESSION['carrito'] as $producto) {
		$total+=$producto['cantidad'] * $producto['precio'];
	}
	$_POST['total']=$total;

	$id = $this->input->post("id");
	$res = $this->order->edit_pedido($id);
	if($res){
		$this->order->quitar_detallePedido($id);
		$this->setDetallePedido($_SESSION['carrito'], $id);
	}

	$_SESSION['carrito']=[];
	$response = [];
	$response["error"] = true;
	echo json_encode($response);
}

private function setDetallePedido($datos, $id){
	if($datos){
		foreach ($datos as $detalle) {
			$this->order->insertar_detallePedido($detalle, $id);
		}
	}
}

	public function delete($id) {
		$response = [];
		$response["error"] = !$this->categorias->delete_categoria($id);
		echo json_encode($response);
	}

public function traer_imagen()
{
	$id=$_POST["id"];
	$response=$this->db->query("select * from metodo_pago where metodo_pago_descripcion='".$id."'")->row_array();
	echo  json_encode($response);exit();
}














}
