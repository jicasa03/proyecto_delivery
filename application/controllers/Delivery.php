<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Delivery extends MY_Controller {

	public $metodo_pago;

	public function __construct() {
		parent::__construct();

		$this->load->model('MetodoPagoModel');
		$this->metodo_pago = new MetodoPagoModel;
	}



	public function index()
	{
		$this->output->set_template('index');
		$this->load->css("public/css/jquery.dataTables.min.css");
		$this->load->js("public/js/jquery.dataTables.min.js");
        $this->load->js("public/js/pedidos/delivery.js");
		$data = [];
		$_SESSION['edit']=null;
		$data['grilla'] = $this->grid();
		$this->load->view('delivery/index',$data);

	}

	private function grid() {
		$this->load->library('datatable');
		$this->datatable->setTabla('tablaDelivery');
		$this->datatable->setNroItem(true);
		$this->datatable->setAttrib('ajax', ['url' => base_url('Delivery/listaGrid')]);
		$this->datatable->setAttrib('language', ['url' => base_url("public/js/spanish.json")]);
		$this->datatable->setAttrib('select', true);
		$this->datatable->setAttrib('dom', 'frtip');
		$this->datatable->setAttrib('processing', false);
		$this->datatable->setAttrib('serverSide', true);
		$this->datatable->setAttrib('responsive', false);
		//$this->datatable->setAttrib('order', [[1, 'desc']]);
		$this->datatable->setAttrib('columns', [
			['title' => 'descripcion', 'data' => "delivery_descripcion", 'name' => "delivery_descripcion"],
			['title' => 'precio', 'data' => "delivery_precio", 'name' => "delivery_precio"],

			
		]);
		return $this->datatable->getJsGrid();
	}
	public function listaGrid() {
		$this->load->library('datatabledb');
		$this->datatabledb->setColumnaId('delivery_id');
		$this->datatabledb->setSelect("delivery_descripcion,delivery_precio");
		$this->datatabledb->setFrom("delivery");
		$this->datatabledb->setWhere("delivery_estado", 1);
		$this->datatabledb->setParams($_GET);
		echo $this->datatabledb->getJson();
	}

	public function store() {
	///	$this->form_validation->set_rules('nombre', 'nombre', 'required');
      //  $this->form_validation->set_rules('email', 'email', 'required');
	//	$this->form_validation->set_rules('usuario', 'usuario', 'required');
		try {
			/*if ($this->form_validation->run() === FALSE) {
				throw new \Exception(validation_errors());
			}*/
			$response = [];
			$id = $this->input->post("id");
			$data=array(
                 "delivery_descripcion"=>$this->input->post("descripcion"),
                 "delivery_precio"=>$this->input->post("monto")
				);


			if ($id === "") {
				
				$res=$this->db->insert("delivery",$data);
				//$res = $this->metodo_pago->insert();
			} else {

				$this->db->where('delivery_id', $id);
			$res = $this->db->update('delivery', $data);
			//$res = $this->metodo_pago->update($id);
			}
			$response["error"] = $res;
			echo json_encode($response);
		} catch (\Exception $e) {
			echo json_encode(["error" => true, "message" => $e->getMessage()]);
		}
	}

	public function get($id) {

		//echo json_encode($this->metodo_pago->find($id));-
		echo json_encode($this->db->get_where('delivery', ['delivery_id' => $id])->row());
	}
		public function delete($id) {
		$response = [];
		///echo "hola0";

		$data = ['delivery_estado' => 0];

		$this->db->where('delivery_id', $id);
		$res= $this->db->update('delivery', $data);
		$response["error"] = !$res;
		echo json_encode($response);
	}



}
?>