<?php
require APPPATH . 'libraries/ImplementJwt.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'phpmailer/src/Exception.php';
require 'phpmailer/src/PHPMailer.php';
require 'phpmailer/src/SMTP.php';
class Recuperarclave extends CI_Controller {
	public $usuarios;
	public function  __construct(){
		parent::__construct(); 
		$this->objOfJwt = new ImplementJwt();
		$this->load->model('usuariosModel');
		$this->usuarios = new UsuariosModel;
	}

	public function index() {
 
		$this->form_validation->set_rules('email', 'email', 'required'); 

		if ($this->form_validation->run()) {
			// exit();
			$usuario = $this->db->get_where('usuario', ['email' => $this->input->post("email")])->row();
			// $usuario =true;
			if($usuario){	
				$time = time();
				$tokenData['iat'] = $time;
				$tokenData['exp'] =  $time + (60*30);
				$tokenData['correo'] = $this->input->post("email");

				$jwtToken = $this->objOfJwt->GenerarToken($tokenData);
				$mensaje = 'Se envio un mensaje de validación a su correo, verifique';
				$html = '<p>Se detecto que intenta cambiar la clave de su cuenta, por favor acceda al siguiente link.</p>
				<p>!Ojo no comparta con nadie, tiene 30 minutos para el cambio de clave :</p>
				<p>link :&nbsp;<a href="'.base_url().'recuperarclave/change/'.$jwtToken.'">aquí</a></p>'; 

				$this->enviar_correo($this->input->post("email"),$tokenData,$html);
				$this->session->set_flashdata('mensaje', $mensaje); 
				$mensaje = 'Se envio mensaje de verificacion a su correo';
					$this->session->set_flashdata('mensaje', $mensaje);
  					redirect(base_url('recuperarclave'), 'refresh');
  				// redirect(base_url('recuperarclave'), 'refresh'); 
			}else{
					$mensaje = 'Correo no valido';
					$this->session->set_flashdata('mensaje', $mensaje);
  					redirect(base_url('recuperarclave'), 'refresh');
  					exit();
			}
		}

		$this->load->view('recuperar');
	}


	public function change($token = ""){
		$data_token = json_decode($this->consultar_token($token),true);
		if ($data_token != null) {
			$_SESSION["tokencorreo"] = $data_token['correo'];
			redirect(base_url('recuperarclave/cambiaclave'), 'refresh');
		}else{
			echo 'xsdsdsd';
		}

	}


	public function cambiaclave(){
		$this->form_validation->set_rules('clave1', 'clave1', 'required'); 
		if ($this->form_validation->run()) { 
			if(isset( $_SESSION["tokencorreo"])){
				$correo = $_SESSION["tokencorreo"];  
				$res = $this->usuarios->update_usuarioclave($correo);
				$mensaje = 'Su clave se ha cambiado';
				$this->session->set_flashdata('mensaje', $mensaje);
				redirect(base_url('Recuperarclave/cambiaclave'), 'refresh'); 
			}else{  
				redirect(base_url('recuperarclave'), 'refresh');
			}
		} 
		$this->load->view('cambiarclave');

	}


	public function consultar_token($token){
		// $header  =  $this->input->request_headers('Authorization');
		$token= $token;
		//echo $tokenparametro;exit();
		try {
			///echo $tokenparametro."";exit();
			$jwtData = $this->objOfJwt->DecodeToken($token); 
			return json_encode($jwtData);
		} catch (Exception $e) {
			$json_encode = "";
 			return  null; 
			// http_response_code('401');
			// echo json_encode(array("status" => false,"message" => $e->getMessage()));exit();
		}
	}

	public function logout() {
		$sesiones = ['usuarioID', 'usuario'];
		$this->session->unset_userdata($sesiones);
		$this->session->sess_destroy();
		redirect(base_url('login'), 'refresh');
	}


	public function enviar_correo($correo,$token,$mensaje){


 $mail = new PHPMailer(true); 
 $dat=array();
  
	  // $mensaje=$mensaje;

      $mail->IsSMTP(); // telling the class to use SMTP
      $mail->Host = "mail.gokiebox.com";
      $mail->SMTPAuth = true; // enable SMTP authentication
      $mail->SMTPKeepAlive = true; // SMTP connection will not close after each email sent

      $mail->Port = 25; // set the SMTP port for the GMAIL server
      $mail->Username = "smtp@gokiebox.com"; // SMTP account username
      $mail->Password = "k2mqpYK,y@ZU"; // SMTP account password
      $mail->SetFrom('smtp@gokiebox.com', 'Procesos');
      $mail->AddReplyTo('smtp@gokiebox.com', '');
      $mail->AddAddress($correo);
       
 
       $mail->Subject="Recuperar clave";//en espass cambiar
       $mail->AltBody=$mensaje;
       $mail->Wordwrap=50;//numero de lineas
       $mail->MsgHTML($mensaje);//formato html para el mensaje
       // $mail->AddAttachment($rutaDeLoAdjuntado);//accedmos al archivo que se ha subido al servidor y lo adjuntamos
       
       if($mail->send()){//si se envio el correo,enviamos el correo
          $respuesta="el mensaje ha sido enviado satisfactoriamente";
          // unlink($rutaDeLoAdjuntado);//para borrar el archivo del servidor
       }else{$respuesta="ERROR:".$mail->ErrorInfo."<br>Ocurrio un error, vuelva a intentarlo porfavor.";}
      return ($respuesta);
    }
}
