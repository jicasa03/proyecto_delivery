<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Metodo_pago extends MY_Controller {

	public $metodo_pago;

	public function __construct() {
		parent::__construct();

		$this->load->model('MetodoPagoModel');
		$this->metodo_pago = new MetodoPagoModel;
	}



	public function index()
	{
		$this->output->set_template('index');
		$this->load->css("public/css/jquery.dataTables.min.css");
		$this->load->js("public/js/jquery.dataTables.min.js");
        $this->load->js("public/js/pedidos/metodo_pago.js");
		$data = [];
		$_SESSION['edit']=null;
		$data['grilla'] = $this->grid();
		$this->load->view('metodo_pago/index',$data);

	}

	private function grid() {
		$this->load->library('datatable');
		$this->datatable->setTabla('tablaMetodoPago');
		$this->datatable->setNroItem(true);
		$this->datatable->setAttrib('ajax', ['url' => base_url('Metodo_pago/listaGrid')]);
		$this->datatable->setAttrib('language', ['url' => base_url("public/js/spanish.json")]);
		$this->datatable->setAttrib('select', true);
		$this->datatable->setAttrib('dom', 'frtip');
		$this->datatable->setAttrib('processing', false);
		$this->datatable->setAttrib('serverSide', true);
		$this->datatable->setAttrib('responsive', false);
		//$this->datatable->setAttrib('order', [[1, 'desc']]);
		$this->datatable->setAttrib('columns', [
			['title' => 'descripcion', 'data' => "metodo_pago_descripcion", 'name' => "metodo_pago_descripcion"],
			
		]);
		return $this->datatable->getJsGrid();
	}
	public function listaGrid() {
		$this->load->library('datatabledb');
		$this->datatabledb->setColumnaId('metodo_pago_id');
		$this->datatabledb->setSelect("metodo_pago_descripcion");
		$this->datatabledb->setFrom("metodo_pago");
		$this->datatabledb->setWhere("metodo_pago_estado", 1);
		$this->datatabledb->setParams($_GET);
		echo $this->datatabledb->getJson();
	}

	public function store() {
	///	$this->form_validation->set_rules('nombre', 'nombre', 'required');
      //  $this->form_validation->set_rules('email', 'email', 'required');
	//	$this->form_validation->set_rules('usuario', 'usuario', 'required');
		try {
			/*if ($this->form_validation->run() === FALSE) {
				throw new \Exception(validation_errors());
			}*/
			$response = [];
			$id = $this->input->post("id");
			if ($id === "") {
				$res = $this->metodo_pago->insert();
			} else {
				$res = $this->metodo_pago->update($id);
			}
			$response["error"] = $res;
			echo json_encode($response);
		} catch (\Exception $e) {
			echo json_encode(["error" => true, "message" => $e->getMessage()]);
		}
	}

	public function get($id) {
		echo json_encode($this->metodo_pago->find($id));
	}
		public function delete($id) {
		$response = [];
		///echo "hola0";
		$response["error"] = !$this->metodo_pago->delete($id);
		echo json_encode($response);
	}



}
?>