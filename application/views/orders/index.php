  	<div class="new_arrivals">
  		<div class="container">
  			<div class="row">
  				<div class="col text-center">
  					<div class="section_title new_arrivals_title">
  						<h2>Nuestra Carta</h2>
  					</div>
  				</div>

  			</div>


 
        <br><br>
        <form id="formulario_busqueda" onsubmit="return llamarwe()">
        <div class="row">
            <div class="col-md-10 col-sm-12"> 
              <input class="form-control" type="text" name="bsqueproducto" id="bsqueproducto" placeholder="Producto a Buscar" >
            </div>
            <div class="col-md-2 col-sm-2">  
              <button class="btn btn-danger col-md-12 col-sm-2" type="submit"> Buscar</button> 
            </div>
        </div>
      </form>

  			<div class="row align-items-center">
  				<div class="col text-center">
  					<div class="new_arrivals_sorting">
  						<ul class="arrivals_grid_sorting clearfix button-group filters-button-group">
                <?php
                  foreach ($categorias as $categoria) {
                    echo '<li class="grid_sorting_button button d-flex flex-column justify-content-center align-items-center" data-filter=".'.str_replace(' ', '', $categoria->descripcion).'">'.$categoria->descripcion.'</li>';
                  }
                 ?>
  						</ul>
  					</div>
  				</div>
  			</div>
  		</div>
  	</div>
<br><br>
<div class="container">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div id="main_menu" class="box_style_2">
          
          </div>
      </div>
    </div>
</div>

    <div class="modal fade" id="modalProducto" role="dialog">
    <form id="formularioDatos" method="POST">
        <div class="modal-dialog modal-large">
            <div class="modal-content">
                <div class="modal-body">
                  <div class="row">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <input type="hidden" name="id" id="id">
                      <input type="hidden" name="precio" id="precio">
                      <span class="tituloProducto"></span>
                      <p class="descripcionProducto"></p>
                      </div>
                  </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-example-wrap mg-t-30">
                                <div class="form-example-int form-horizental">
                                    <div class="form-group">
                                        <div class="row">
                                          <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                                <label class="labelCantidad" for="cantidad">Cantidad: </label>
                                          </div>
                                          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <input type="number" id="cantidad" name="cantidad" class="form-control input-sm" value="1" min="1">
                                          </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="nk-int-st">
                                                    <label for="cantidad">Nota Adicional: </label>
                                                    <textarea class="form-control input-sm" id="nota" name="nota" rows="2"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="divAtributos">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="btn btn-success btn-block btn-lg" id="botonAgregar">
                              <div class="precioProducto">
                                S/ <span class="subTotalPro"></span>
                              </div>
                              <div class="vertical-separator">

                              </div>
                              <div class="textoAgregar">
                                Agregar al pedido
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    </div>


  <!-- Benefit -->

  <div class="benefit">
    <div class="container">
      <div class="row benefit_row">
        <div class="col-lg-3 benefit_col">
          <div class="benefit_item d-flex flex-row align-items-center">
            <div class="benefit_icon"><i class="fa fa-motorcycle" aria-hidden="true"></i></div>
            <div class="benefit_content">
              <h6>Costo de Envío</h6>
              <p> <?php  
             // echo $precio_delivery."<br>";
                             if((float)$precio_delivery!=0){
                              if($tam_precio>1){
                                echo "desde ";
                              }
                             echo "s/ ".number_format($precio_delivery, 2, '.', '');

                             }else{
                              if($tam_precio>1){
                                echo "desde S/0.00 (gratis) ";
                              }else{
                              echo "Es gratis";

                              }
                             }



               ?> en todos sus pedidos</p>
            </div>
          </div>
        </div>
        <div class="col-lg-3 benefit_col">
          <div class="benefit_item d-flex flex-row align-items-center">
            <div class="benefit_icon"><i class="fa fa-money" aria-hidden="true"></i></div>
            <div class="benefit_content">
              <h6>Recibe y pague</h6>
              <p>cuando se entregue su pedido</p>
            </div>
          </div>
        </div>
        <div class="col-lg-3 benefit_col">
          <div class="benefit_item d-flex flex-row align-items-center">
            <div class="benefit_icon"><i class="fa fa-undo" aria-hidden="true"></i></div>
            <div class="benefit_content">
              <h6>Delivery Rápido</h6>
              <p>como se merece</p>
            </div>
          </div>
        </div>
        <div class="col-lg-3 benefit_col">
          <div class="benefit_item d-flex flex-row align-items-center">
            <div class="benefit_icon"><i class="fa fa-clock-o" aria-hidden="true"></i></div>
            <input type="hidden" name="horario_entrada" id="horario_entrada" value="<?php echo $_SESSION['horario_entrada'] ?>">
            <input type="hidden" name="horariosalida" id="horariosalida" value="<?php echo $_SESSION['horario_salida'] ?>">
            <div class="benefit_content">
              <h6>Horario de Reparto</h6>
              <p><?= $_SESSION['horario_entrada'] ?> - <?= $_SESSION['horario_salida'] ?></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<br>
<br>