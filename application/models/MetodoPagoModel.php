<?php


class MetodoPagoModel extends CI_Model {
	public function __construct() {
		parent::__construct();
		$this->load->database(); 
	}


	public function insert() {


		$imagen="";

		if (!empty($_FILES['archivo']['name'])) {
         	$config= 'public/uploads/productos/';

         	$fichero_subido = $config . basename($_FILES['archivo']['name']);

				//echo '<pre>';
				if (move_uploaded_file($_FILES['archivo']['tmp_name'], $fichero_subido)) {
				   // echo "El fichero es válido y se subió con éxito.\n";
				} else {
				//    echo "¡Posible ataque de subida de ficheros!\n";
				}

				$imagen=basename($_FILES['archivo']['name']);

		}

		//$image
		$data = [
			'metodo_pago_descripcion'    => $this->input->post('nombre'),
			'metodo_pago_qr'=>$imagen,
			'metodo_pago_tipo'=>2
		
		];
		return $this->db->insert('metodo_pago', $data);
	}
	public function update($id) {

			$imagen="";

		if (!empty($_FILES['archivo']['name'])) {
         	$config= 'public/uploads/productos/';

         	$fichero_subido = $config . basename($_FILES['archivo']['name']);

				//echo '<pre>';
				if (move_uploaded_file($_FILES['archivo']['tmp_name'], $fichero_subido)) {
				   // echo "El fichero es válido y se subió con éxito.\n";
				} else {
				//    echo "¡Posible ataque de subida de ficheros!\n";
				}

				$imagen=basename($_FILES['archivo']['name']);

		}

      if($imagen!=""){
			$data = [
						'metodo_pago_descripcion'    => $this->input->post('nombre'),
						'metodo_pago_qr'=>$imagen
					
					];

      }else{
      	$data = [
      		    'metodo_pago_descripcion'    => $this->input->post('nombre')
				];


      }
		

		$this->db->where('metodo_pago_id', $id);
			return $this->db->update('metodo_pago', $data);


	}


	public function find_usuario($id) {
		return $this->db->get_where('usuario', ['id' => $id])->row();
	}

	public function getTipoUsuarios() {
		$this->db->select('*');
        $this->db->from('tipo_usuario');
        $this->db->where('estado=1');
        $q = $this->db->get();
        return $q->result();
	}


	public function update_usuario($id) {
		$data = [
			'nombre'    => $this->input->post('nombre'),
			'usuario'    => $this->input->post('usuario'),
			'telefono'      => $this->input->post('telefono'),
			'email'      => $this->input->post('email'),
		];
		if ($this->input->post("password") !== "") {
			$data["password"] = password_hash($this->input->post("password"), PASSWORD_DEFAULT);
		}

		if ($id == 0) {
			return $this->db->insert('usuario', $data);
		} else {
			$this->db->where('id', $id);
			return $this->db->update('usuario', $data);
		}
	}

	public function update_usuarioclave($correo) {
 
		if ($this->input->post("password") !== "") {
			$data["password"] = password_hash($this->input->post("clave1"), PASSWORD_DEFAULT);
		}
 
			$this->db->where('email', $correo);
			return $this->db->update('usuario', $data); 
	}

	public function delete($id) {
		$data = ['metodo_pago_estado' => 0];

		$this->db->where('metodo_pago_id', $id);
		return $this->db->update('metodo_pago', $data);
	}

		public function find($id) {
		return $this->db->get_where('metodo_pago', ['metodo_pago_id' => $id])->row();
	}


}
