$(()=>{


});

$('#btn_add').click(function (e) {
	e.preventDefault();
	$('#formularioDatos')[0].reset();
	$('#usuarioModal').modal('show');
});

$('#formularioDatos').submit(function (e) {
e.preventDefault();
	let parametros = new FormData($('#formularioDatos')[0]);
	$.ajax({
		url: base_url + 'Metodo_pago/store',
		type: 'POST',
		data: parametros,
		dataType: 'json',
		cache: false,
		contentType: false,
		processData: false,
		success: function (data) {
			if (data.error  == 3) {
				toastr.error('N° DE ORDEN REPETIDO', 'ERROR!');
			} else if(data.error){
				toastr.success('SE GUARDÓ CORRECTAMENTE', 'EXCELENTE!');
				$('#usuarioModal').modal('hide');
				tablaMetodoPago.ajax.reload();
			}else{
				toastr.error('NO SE PUDO AGREGAR', 'ERROR!');
			}
		},
	});

});

$('#btn_delete').click(function (e) {
	e.preventDefault();
	let indexR = tablaMetodoPago.row('.selected').id();
	if (indexR != null) {
		eliminar(indexR);
	} else {
		toastr.error('SELECCIONA UN REGISTRO PRIMERO', 'ERROR!');
	}
});


function eliminar (id) {

    if(id.toString()=="1"){
toastr.error('NO SE PUDO ELIMINAR', 'ERROR!');
return false;
    }

	swal({
		title: '¿Estás seguro?',
		text: 'No se podrá recuperar los datos eliminados!',
		icon: 'warning',
		buttons: true,
		dangerMode: true,
		showLoaderOnConfirm: true,
	}).then((willDelete) => {
		if (willDelete) {
			$.post(base_url + 'Metodo_pago/delete/' + id, function (data) {
				if (!data.error) {
					toastr.success('SE ELIMINÓ CORRECTAMENTE', 'EXCELENTE!');
					tablaMetodoPago.ajax.reload();
				} else {
					toastr.error('NO SE PUDO ELIMINAR', 'ERROR!');
				}
			}, 'json');
		}
	});
}

$('#btn_edit').click(function (e) {
	$('.labelid').show(); 

	e.preventDefault();
	$('#formularioDatos')[0].reset();
	let indexR = tablaMetodoPago.row('.selected').id();
	if (indexR != null) {
		editar(indexR);
	} else {
		toastr.error('SELECCIONA UN REGISTRO PRIMERO', 'ERROR!');
	}
});
function editar (id) {
	$.post(base_url + 'Metodo_pago/get/' + id, function (data) {
		$('#id').val(data.metodo_pago_id);
		$('#nombre').val(data.metodo_pago_descripcion);
		/*$('#precio').val(data.precio);
		$('#descripcion').val(data.descripcion);
		$('#orden').val(data.orden);
		setAtributosEdit(data['atributos']);
		setCategoriesEdit(data['categories']);*/
		$('#usuarioModal').modal('show');
	}, 'json');
}